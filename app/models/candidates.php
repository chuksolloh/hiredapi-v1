<?php

namespace models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Description of candidates
 *
 * @author chuksolloh
 */

class Candidates extends Eloquent {
	//put your code here
	
	public function education() {
		return $this->hasMany('Education');
	}
}

<?php

use models\Candidates;

$app->group('/v1', function() use($app){
	// All candidates
	$app->get('/candidates', function() use($app){
		$app->response()->body(Candidates::all() ? : []);
	});
	
	// Specific candidate
	$app->get('/candidates/id', function($id) use($app){
		$app->response()->body(Candidates::where('id_candidate', $id)->first() ? : []);
	});
});

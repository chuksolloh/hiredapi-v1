<?php

use \models\Company;

$app->group('/v1', function() use($app){
	// All companies
	$app->get('/companies', function() {
		$company = Company::all();
		echo $company;
	});

	// Company by id
	$app->get('/companies/:id', function($id){
		$company = Company::where('id_company', $id)->first();
		echo $company;
	});
});

<?php

use Illuminate\Database\Capsule\Manager as Capsule;

require '../vendor/autoload.php';
require 'database.php';

$app = new \Slim\Slim();

// Config settings
$app->config(array(
	'log.level' => \Slim\Log::ERROR,
	'mode' => 'development'
));

$app->contentType('application/json'); // Return json encoded data

$log = $app->getLog();
$log->setLevel(\Slim\Log::WARN);

// Production mode
$app->configureMode('production', function() use($app){
	$app->config(array(
		'log.enable' => true,
		'debug' => true
	));
});

// Development mode
$app->configureMode('development', function() use($app){
	$app->config(array(
		'log.enable' => false,
		'debug' => true
	));
});

$app->db = function() {
	return new Capsule;
};

require 'routes/companies/profile.php';
require 'routes/candidates/profile.php';
